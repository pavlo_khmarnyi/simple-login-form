package com.example.cloudme.simple_login_form.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.cloudme.simple_login_form.R;
import com.example.cloudme.simple_login_form.model.Gender;
import com.example.cloudme.simple_login_form.model.User;
import com.example.cloudme.simple_login_form.repository.UserRepository;
import com.example.cloudme.simple_login_form.repository.UserRepositoryImpl;
import com.example.cloudme.simple_login_form.util.LogType;
import com.example.cloudme.simple_login_form.util.Logger;

public class RegisterActivity extends AppCompatActivity {

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mEmailEditText;
    private RadioButton mMaleRadioButton;
    private RadioButton mFemaleRadioButton;
    private EditText mPasswordEditText;
    private EditText mConfirmPasswordEditText;
    private Button mSignUpButton;

    private UserRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_avtivity);

        repository = new UserRepositoryImpl(this);

        mFirstNameEditText = findViewById(R.id.firstNameEditText);
        mLastNameEditText = findViewById(R.id.lastNameEditText);
        mEmailEditText = findViewById(R.id.emailEditText);
        mMaleRadioButton = findViewById(R.id.maleRadioButton);
        mFemaleRadioButton = findViewById(R.id.femaleRadioButton);
        mPasswordEditText = findViewById(R.id.passwordEditText);
        mConfirmPasswordEditText = findViewById(R.id.confirmPasswordEditText);
        mSignUpButton = findViewById(R.id.signUpButton);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstName = mFirstNameEditText.getText().toString();
                String lastName = mLastNameEditText.getText().toString();
                String email = mEmailEditText.getText().toString();
                String gender = Gender.MALE.name();
                String password = mPasswordEditText.getText().toString();
                String confirmPassword = mConfirmPasswordEditText.getText().toString();

                User user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setGender(gender);
                user.setPassword(password);

                try {
                    repository.saveUser(user);
                    Logger.log(LogType.REGISTER_ACTIVITY_LOG, getResources().getString(R.string.user_is_saved_successfully));
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.user_is_saved_successfully), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Logger.error(LogType.REGISTER_ACTIVITY_LOG, getResources().getString(R.string.user_is_not_saved));
                    Logger.log(LogType.REGISTER_ACTIVITY_LOG,getResources().getString(R.string.error) + e.getMessage());
                    Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        Logger.log(LogType.REGISTER_ACTIVITY_LOG, getResources().getString(R.string.register_activity_is_created));
    }
}
