package com.example.cloudme.simple_login_form.util;

import android.util.Log;

public class Logger {
    public static void log(LogType logType, String message) {
        Log.d(logType.getCode(), message);
    }

    public static void error(LogType logType, String message) {
        Log.e(logType.getCode(), message);
    }
}
