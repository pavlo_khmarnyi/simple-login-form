package com.example.cloudme.simple_login_form.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.cloudme.simple_login_form.model.User;
import com.example.cloudme.simple_login_form.util.DatabaseConfig;

@Database(entities = {User.class}, version = DatabaseConfig.DATABASE_VERSION)
public abstract class UserDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
