package com.example.cloudme.simple_login_form.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.cloudme.simple_login_form.model.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM User WHERE email LIKE :email AND password LIKE :password")
    User findUserByEmailAndPassword(String email, String password);

    @Insert
    void insertUser(User user);

    @Update
    void updateUser(User user);
}
