package com.example.cloudme.simple_login_form.model;

public enum Gender {
    MALE("MALE"),
    FEMALE("FEMALE");

    String code;

    Gender(String code) {
        this.code = code;
    }

    String getCode() {
        return this.code;
    }
}
