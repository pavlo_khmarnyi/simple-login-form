package com.example.cloudme.simple_login_form.presenter;

public interface LoginActivityPresenter {
    boolean checkUser(String login, String password);
    boolean isUserExist(String login, String password);
}
