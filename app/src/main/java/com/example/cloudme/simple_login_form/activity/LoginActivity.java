package com.example.cloudme.simple_login_form.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.cloudme.simple_login_form.R;
import com.example.cloudme.simple_login_form.presenter.LoginActivityPresenter;
import com.example.cloudme.simple_login_form.presenter.LoginActivityPresenterImpl;
import com.example.cloudme.simple_login_form.util.LogType;
import com.example.cloudme.simple_login_form.util.Logger;
import com.example.cloudme.simple_login_form.view.LoginActivityView;

public class LoginActivity extends AppCompatActivity implements LoginActivityView {

    private LoginActivityPresenter presenter;

    private Button mLoginButton;
    private Button mRegisterButton;
    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private TextView mIncorrectLoginTextView;
    private ProgressBar mCheckLoginAndPasswordProgressBar;

    private String login;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        mLoginButton = findViewById(R.id.loginButton);
        mRegisterButton = findViewById(R.id.registerButton);
        mLoginEditText = findViewById(R.id.loginEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);
        mIncorrectLoginTextView = findViewById(R.id.incorrectLoginTextView);
        mCheckLoginAndPasswordProgressBar = findViewById(R.id.checkLoginProgressBar);

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegisterActivity();
            }
        });

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login = mLoginEditText.getText().toString();
                password = mPasswordEditText.getText().toString();
                if (presenter.checkUser(login, password)) {
                    goToUserDataActivity();
                }
            }
        });

        presenter = new LoginActivityPresenterImpl(this);
    }

    private void goToRegisterActivity() {
        Logger.log(LogType.LOGIN_ACTIVITY_LOG, getResources().getString(R.string.go_to_register_activity));
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void goToUserDataActivity() {
        Logger.log(LogType.LOGIN_ACTIVITY_LOG, getResources().getString(R.string.go_to_user_data_activity));
        Intent intent = new Intent(this, UserDataActivity.class);
        startActivity(intent);
    }

    @Override
    public void showWrongLoginOrPasswordMessage() {
        mIncorrectLoginTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWrongLoginOrPasswordMessage() {
        mIncorrectLoginTextView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showCheckLoginOrPasswordProgressBar() {
        mCheckLoginAndPasswordProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCheckLoginOrPasswordProgressBar() {
        mCheckLoginAndPasswordProgressBar.setVisibility(View.INVISIBLE);
    }
}
