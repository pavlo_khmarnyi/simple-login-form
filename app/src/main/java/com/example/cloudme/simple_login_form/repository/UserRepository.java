package com.example.cloudme.simple_login_form.repository;

import com.example.cloudme.simple_login_form.model.User;

public interface UserRepository {
    void saveUser(User user);
    void updateUser(User user);
    User getUser(String email, String password);
}
