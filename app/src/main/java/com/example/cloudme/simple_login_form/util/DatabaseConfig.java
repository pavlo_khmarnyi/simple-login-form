package com.example.cloudme.simple_login_form.util;

public class DatabaseConfig {
    public static final String DATABASE_NAME = "user";
    public static final int DATABASE_VERSION = 1;
}
