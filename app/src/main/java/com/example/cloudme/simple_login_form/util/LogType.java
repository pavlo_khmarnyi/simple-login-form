package com.example.cloudme.simple_login_form.util;

public enum LogType {
    LOGIN_ACTIVITY_LOG("LoginActivity log: "),
    LOGIN_PRESENTER_LOG("LoginPresenter log: "),
    REGISTER_ACTIVITY_LOG("RegisterActivity log: "),
    REGISTER_PRESENTER_LOG("RegisterPresenter log: "),
    USER_DATA_ACTIVITY_LOG("UserDataActivity log: "),
    USER_DATA_PRESENTER_LOG("UserDataPresenter log: ");

    private String code;

    LogType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
