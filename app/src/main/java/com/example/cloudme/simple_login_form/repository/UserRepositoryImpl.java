package com.example.cloudme.simple_login_form.repository;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.cloudme.simple_login_form.database.UserDatabase;
import com.example.cloudme.simple_login_form.model.User;
import com.example.cloudme.simple_login_form.util.DatabaseConfig;

public class UserRepositoryImpl implements UserRepository {

    private UserDatabase database;

    public UserRepositoryImpl(Context context) {
        database = Room.databaseBuilder(context, UserDatabase.class, DatabaseConfig.DATABASE_NAME).
                allowMainThreadQueries().
                build();
    }

    @Override
    public void saveUser(User user) {
        database.userDao().insertUser(user);
    }

    @Override
    public void updateUser(User user) {
        database.userDao().updateUser(user);
    }

    @Override
    public User getUser(String email, String password) {
        return database.userDao().findUserByEmailAndPassword(email, password);
    }
}
