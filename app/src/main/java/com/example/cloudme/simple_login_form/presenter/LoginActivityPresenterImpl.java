package com.example.cloudme.simple_login_form.presenter;

import com.example.cloudme.simple_login_form.util.LogType;
import com.example.cloudme.simple_login_form.util.Logger;
import com.example.cloudme.simple_login_form.view.LoginActivityView;

public class LoginActivityPresenterImpl implements LoginActivityPresenter {

    private final static String LOGIN_OR_PASSWORD_IS_INCORRECT = "Login or password is incorrect!";
    private final static String USER_DOES_NOT_EXIST = "User does not exist!";

    private LoginActivityView view;

    public LoginActivityPresenterImpl(LoginActivityView view) {
        this.view = view;
        Logger.log(LogType.LOGIN_PRESENTER_LOG, "LoginPresenter created!");
    }

    @Override
    public boolean checkUser(String login, String password) {
        if (login == "" || password == "" || login == " " || password == " ") {
            view.hideCheckLoginOrPasswordProgressBar();
            view.showWrongLoginOrPasswordMessage();
            Logger.error(LogType.LOGIN_PRESENTER_LOG, LOGIN_OR_PASSWORD_IS_INCORRECT);
            return false;
        }

        if (!isUserExist(login, password)) {
            Logger.error(LogType.LOGIN_PRESENTER_LOG, USER_DOES_NOT_EXIST);
            view.hideCheckLoginOrPasswordProgressBar();
            view.showWrongLoginOrPasswordMessage();
            return false;
        }

        return true;
    }

    @Override
    public boolean isUserExist(String login, String password) {
        return false;
    }
}
