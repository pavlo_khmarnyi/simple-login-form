package com.example.cloudme.simple_login_form.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.example.cloudme.simple_login_form.R;

public class UserDataActivity extends AppCompatActivity {
    private TextView mShowFirstNameTextView;
    private TextView mShowLastNameTextView;
    private TextView mShowemeilTextView;
    private TextView mShowgenderTextView;
    private Button editButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userdata_activity);

        mShowFirstNameTextView = findViewById(R.id.showFirstNameTextView);
        mShowLastNameTextView = findViewById(R.id.showLastNameTextView);
        mShowemeilTextView = findViewById(R.id.showemeilTextView);
        mShowgenderTextView = findViewById(R.id.showgenderTextView);
        editButton = findViewById(R.id.editButton);
    }
}
