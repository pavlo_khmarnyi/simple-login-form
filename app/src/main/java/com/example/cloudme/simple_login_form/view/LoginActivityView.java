package com.example.cloudme.simple_login_form.view;

public interface LoginActivityView {
    void showWrongLoginOrPasswordMessage();
    void hideWrongLoginOrPasswordMessage();
    void showCheckLoginOrPasswordProgressBar();
    void hideCheckLoginOrPasswordProgressBar();
}
